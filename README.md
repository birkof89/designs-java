# Designs Java

## https://birkof.com

This is public repo where you can find source codes of different designs in Java language. Follow this link to [read more design articles](https://birkof.com/blog/) on my blog.