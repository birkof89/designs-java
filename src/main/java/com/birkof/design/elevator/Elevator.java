package com.birkof.design.elevator;

import com.birkof.design.elevator.enums.ElevatorState;
import java.io.Serializable;
import java.util.LinkedList;

class Elevator implements Serializable {
    private Integer id;
    private ElevatorState state;
    private Integer currentFloor;
    private LinkedList stopFloorList = new LinkedList<Integer>();
    Elevator(ElevatorState state, Integer currentFloor, Integer id){
        this.state = state;
        this.id = id;
        this.currentFloor = currentFloor;
    }
    public boolean addStop(Integer newStopFloor){
        System.out.println("Elevator "+ this.id +" Adding stop floor "+newStopFloor);
        if(this.stopFloorList.contains(newStopFloor)){
            return true;
        }
        if(
                this.stopFloorList.isEmpty() ||
                (this.stopFloorList.isEmpty() || this.state == ElevatorState.MOVING_UP && this.currentFloor > newStopFloor) ||
                        (this.state == ElevatorState.MOVING_DOWN && this.currentFloor < newStopFloor) ||
                        this.state == ElevatorState.IDLE
        ){
            //adding new stop to the end of the queue
            this.stopFloorList.add(newStopFloor);
        }else{
            LinkedList tmpStopFloorList = new LinkedList<Integer>();
            for (int i = 0; i < this.stopFloorList.size(); i++){
                //moving up
                if(this.state.equals(ElevatorState.MOVING_UP) && newStopFloor <= (Integer) this.stopFloorList.get(i)){
                    tmpStopFloorList.add(newStopFloor);
                }
                //moving down
                if(this.state.equals(ElevatorState.MOVING_DOWN) && newStopFloor >= (Integer) this.stopFloorList.get(i)){
                    tmpStopFloorList.add(newStopFloor);
                }
                //case for last stop in list
                if(!tmpStopFloorList.contains(newStopFloor) && i == this.stopFloorList.size() - 1){
                    tmpStopFloorList.add(newStopFloor);
                }
                tmpStopFloorList.add((Integer) this.stopFloorList.get(i));
            }
            this.stopFloorList.clear();
            this.stopFloorList = tmpStopFloorList;
        }
        return true;
    }
    public boolean setState(ElevatorState state){
        this.state = state;
        return true;
    }
    public ElevatorState getState(){
        return this.state;
    }
    public boolean setCurrentFloor(Integer currentFloor){
        this.currentFloor = currentFloor;
        return true;
    }
    public LinkedList getStopFloorList(){
        return this.stopFloorList;
    }
    public Integer getCurrentFloor(){
        return this.currentFloor;
    }
    public Integer getId(){
        return this.id;
    }
    public Integer getReachingFloorTime(Integer callFloor){
        Integer time = 0;

        if(
                (this.state == ElevatorState.MOVING_UP && this.currentFloor <= callFloor) ||
                        (this.state == ElevatorState.MOVING_DOWN && this.currentFloor >= callFloor)
        ){
            time += Math.abs(this.currentFloor - callFloor);
        }else {
            if(this.stopFloorList.size() > 0){
                //current floor -> last stop floor -> call floor
                time += Math.abs((Integer) this.stopFloorList.peekLast() - callFloor) +
                        Math.abs((Integer) this.stopFloorList.peekLast() - this.currentFloor);
            }else{
                // elevator is idling
                time += Math.abs(this.currentFloor - callFloor);
            }
        }
        System.out.println("Elevator "+this.id+ " will arrive at "+time);
        return time;
    }
}
