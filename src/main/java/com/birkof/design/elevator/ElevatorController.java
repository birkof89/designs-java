package com.birkof.design.elevator;

import com.birkof.design.elevator.enums.ElevatorState;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class ElevatorController{
    private HashMap<Integer,Elevator> elevators = new HashMap<Integer, Elevator>();
    ElevatorController(Integer elevatorAmount){
        for (int i=1; i<=elevatorAmount; i++){
            elevators.put(i,new Elevator(ElevatorState.IDLE,0,i));
        }
    }
    public boolean addStopInternally(Integer newStop, Integer elevatorId){
        return this.elevators.get(elevatorId).addStop(newStop);
    }
    public int pickupElevator(Integer floor){
        Integer minTime = null;
        Integer minId = null;
        for(Elevator elevator : this.elevators.values()){
            if(minTime == null || elevator.getReachingFloorTime(floor) < minTime){
                minId = elevator.getId();
                minTime = elevator.getReachingFloorTime(floor);
            }
        }
        this.elevators.get(minId).addStop(floor);
        return minId;
    }
    public boolean saveElevatorsState(){
        try {
            for(Elevator elevator : this.elevators.values()){
                FileOutputStream fileOutputStream = new FileOutputStream("/tmp/elevator_"+elevator.getId());
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                objectOutputStream.writeObject(elevator);
            }
        }catch (Exception exception){
            System.out.println(exception.getMessage());
            return false;
        }
        return true;
    }
    public boolean restoreElevatorsState(){
        try {
            for(Elevator elevator : this.elevators.values()){
                FileInputStream fileInputStream = new FileInputStream("/tmp/elevator_"+elevator.getId());
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                elevators.replace(elevator.getId(), (Elevator) objectInputStream.readObject());
            }
        }catch (Exception exception){
            System.out.println(exception.getMessage());
            return false;
        }
        return true;
    }
}
