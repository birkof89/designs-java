package com.birkof.design.elevator.enums;

public enum ElevatorState {
    IDLE,
    MOVING_UP,
    MOVING_DOWN
}
