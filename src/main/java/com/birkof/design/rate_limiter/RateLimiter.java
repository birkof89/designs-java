package com.birkof.design.rate_limiter;
import java.util.Date;
class RateLimiter{
    private final Integer allowedBucketSize,refillAfterMs;
    private Integer currentBucketSize;
    private Long lastRefillTime;
    RateLimiter(Integer allowedBucketSize, Integer refillAfter){
        this.allowedBucketSize = allowedBucketSize;
        this.refillAfterMs = refillAfter;
        this.refill();
    }
    public boolean refill(){
        if(this.lastRefillTime == null){
            this.lastRefillTime = new Date().getTime();
            this.currentBucketSize = 0;
            return true;
        }else if(this.currentBucketSize > 0){
            System.out.println("Refill is allowed");
            Double refillAmount = Double.valueOf(new Date().getTime() - this.lastRefillTime) / this.refillAfterMs * this.allowedBucketSize;
            if(refillAmount>0){
                this.currentBucketSize -= refillAmount.intValue();
                if(this.currentBucketSize < 0){
                    this.currentBucketSize = 0;
                };
                this.lastRefillTime = new Date().getTime();
                System.out.println("Current bucket size "+this.currentBucketSize);
                return true;
            }
        }
        return false;
    }
    public boolean allowRequest(){
        this.refill();
        this.currentBucketSize++;
        if(this.currentBucketSize <= this.allowedBucketSize){
            return true;
        }
        return false;
    }
    public Long getLastRefillTime(){
        return this.lastRefillTime;
    }
    public Integer getCurrentBucketSize() {
        return currentBucketSize;
    }
    public Integer getAllowedBucketSize(){
        return this.allowedBucketSize;
    }
    public Integer getRefillAfterMs(){
        return this.refillAfterMs;
    }
}
