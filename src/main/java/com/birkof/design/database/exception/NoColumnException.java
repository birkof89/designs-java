package com.birkof.design.database.exception;

public class NoColumnException extends Exception{
    public NoColumnException(String column){
        super("[No column exception]  "+ column + " does not exist");
    }
}
