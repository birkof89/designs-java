package com.birkof.design.database;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;

public class DatabaseManager{
    private HashMap<String, Database> databases = new HashMap<String, Database>();
    public boolean createDatabase(String dbName){
        System.out.println("--Created DB "+dbName);
        this.databases.put(dbName,new Database(dbName));
        return true;
    }
    public boolean deleteDatabase(String dbName){
        if(this.databases.containsKey(dbName)){
            this.databases.remove(dbName);
            System.out.println("--Removed DB "+dbName);
            return true;
        }
        return false;
    }
    public Database useDatabase(String dbName){
        if(this.databases.containsKey(dbName)){
            System.out.println("--Using database "+dbName);
            return this.databases.get(dbName);
        }
        System.out.println("--No database founded");
        return null;
    }
    public boolean saveDBState(){
        try {
            for(Database db : this.databases.values()){
                FileOutputStream fileOutputStream = new FileOutputStream("/tmp/db_"+db.getName());
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                objectOutputStream.writeObject(db);
            }
        }catch (Exception exception){
            System.out.println(exception.getMessage());
            return false;
        }

        return true;
    }
    public boolean restoreDBState(){
        try {
            for(Database db : this.databases.values()){
                FileInputStream fileInputStream = new FileInputStream("/tmp/db_"+db.getName());
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                databases.replace(db.getName(), (Database) objectInputStream.readObject());
            }
        }catch (Exception exception){
            System.out.println(exception.getMessage());
            return false;
        }
        return true;
    }
}
