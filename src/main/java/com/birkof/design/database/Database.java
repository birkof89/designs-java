package com.birkof.design.database;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

public class Database implements Serializable {
    private final String name;
    private HashMap<String, Table> tables = new HashMap<String, Table>();
    private final Long createdAt;
    private Long updatedAt;
    public Database(String name){
        this.name = name;
        this.createdAt = this.updatedAt = (new Date()).getTime();
    }
    public boolean createTable(String tableName){
        this.tables.put(tableName, new Table(tableName));
        this.setUpdatedAt();
        System.out.println("--Created table "+tableName);
        return true;
    }
    public boolean removeTable(String tableName){
        if(!this.tables.isEmpty()){
            this.tables.remove(tableName);
            this.setUpdatedAt();
            System.out.println("--Removed table "+name);
            return true;
        }
        return false;
    }
    public HashMap<String, Table> getTables(){
        return this.tables;
    }
    public Table getTable(String tableName){
        if(this.tables.containsKey(tableName)){
            return this.tables.get(tableName);
        }
        return null;
    }
    public String getName(){
        return this.name;
    }
    public Long getCreatedAt(){
        return this.createdAt;
    }
    private void setUpdatedAt(){
        this.updatedAt = (new Date()).getTime();
    }
    public Long getUpdatedAt(){
        return this.updatedAt;
    }
}