package com.birkof.design.database;

import com.birkof.design.database.exception.NoColumnException;
import java.util.Date;
import java.util.HashMap;

public class Row {
    private final String id;
    private HashMap<String, String> keyValueMap = new HashMap<String, String>();
    private final Long createdAt;
    private Long updatedAt;
    public Row(String id, HashMap<String, String> keyValueMap){
        this.id = id;
        if(keyValueMap != null){
            this.keyValueMap = keyValueMap;
        }
        this.createdAt = this.updatedAt = (new Date()).getTime();
    }
    public String getId(){
        return this.id;
    }
    public Long getCreatedAt(){
        return this.createdAt;
    }
    private void setUpdatedAt(){
        this.updatedAt = (new Date()).getTime();
    }
    public Long getUpdatedAt(){
        return this.updatedAt;
    }
    public void setKeyValueMap(HashMap<String,String> keyValueMap){
        this.keyValueMap = keyValueMap;
        this.setUpdatedAt();
    }
    public HashMap<String, String> getKeyValueMap(){
        return this.keyValueMap;
    }
    public void setColumnValue(String column, String value){
        if(this.keyValueMap.containsKey(column)){
            this.keyValueMap.replace(column, value);
        }else{
            this.keyValueMap.put(column, value);
        }
        this.setUpdatedAt();
    }
    public String getColumnValue(String column) throws NoColumnException {
        if(this.keyValueMap.containsKey(column)){
            return this.keyValueMap.get(column);
        }
        throw new NoColumnException(column);
    }
}