package com.birkof.design.database;

import com.birkof.design.database.exception.NoColumnException;

import java.util.Date;
import java.util.HashMap;
public class Table{
    private final String name;
    private HashMap<String,Row> rows = new HashMap<String, Row>();
    private HashMap<Integer, HashMap> transactions = new HashMap<>();
    private final Long createdAt;
    private Long updatedAt;
    public Table(String name){
        this.name = name;
        this.createdAt = this.updatedAt = (new Date()).getTime();
    }
    private String generateRowId(){
        return Double.toHexString(Math.random());
    }
    public String getName(){
        return this.name;
    }
    public String insertRow(HashMap<String, String> rowData){
        String rowId = this.generateRowId();
        this.rows.put(rowId, new Row(rowId,rowData));
        this.setUpdatedAt();
        return rowId;
    }
    public Row selectRowById(String rowId){
        if(this.rows.containsKey(rowId)){
            return this.rows.get(rowId);
        }
        System.out.println("No row founded");
        return null;
    }
    public boolean updateRowById(String rowId,HashMap<String,String> data){
        if(this.rows.containsKey(rowId)){
            System.out.println("--Updating row");
            Row row = this.rows.get(rowId);
            data.forEach((key,value)->{
                row.setColumnValue(key,value);
            });
            this.setUpdatedAt();
            return true;
        }
        System.out.println("No row founded");
        return false;
    }
    public boolean deleteRowById(String rowId){
        if(this.rows.containsKey(rowId)){
            System.out.println("--Deleting row");
            this.setUpdatedAt();
            this.rows.remove(rowId);
            return true;
        }
        return false;
    }
    public Integer countRowsByKey(String countKey){
        Integer counter = 0;
        if (this.rows.size() > 0){

            for (Row row : this.rows.values()){
                try{
                    String value = row.getColumnValue(countKey);
                    counter++;
                }catch (NoColumnException exception){
                    //Do nothing when counting;
                }

            }
        }
        return counter;
    }
    public Integer countRowsByValue(String countValue){
        Integer counter = 0;
        if (this.rows.size() > 0){
            for (Row row : this.rows.values()){
                if(row.getKeyValueMap().containsValue(countValue)){
                    counter++;
                }
            }
        }
        return counter;
    }
    public HashMap<String, Row> getRows(){
        return this.rows;
    }
    public void startTransaction(){
        HashMap<String, Row> transactionRows = new HashMap<>();

        for (Row row : this.rows.values()){
            HashMap<String,String> copyData = new HashMap<>();
            row.getKeyValueMap().forEach((k,v)->{
                copyData.put(k,v);
            });
            transactionRows.put(row.getId(),new Row(row.getId(),copyData));
        }
        this.transactions.put(this.transactions.size()+1,transactionRows);
    }
    public boolean commitTransaction(){
        if(!this.transactions.isEmpty()){
            if(this.transactions.remove(this.transactions.size()) != null){
                return true;
            }
        }
        System.out.println("No opened transaction exists for commit");
        return false;
    }
    public boolean rollbackTransaction(){
        if(!this.transactions.isEmpty()){
            this.rows.clear();
            HashMap<String, Row> transactionRows = (HashMap<String, Row>) this.transactions.get(this.transactions.size());
            for (Row row : transactionRows.values()) {
                this.rows.put(row.getId(), new Row(row.getId(), row.getKeyValueMap()));
            }
            this.transactions.remove(this.transactions.size());
            return true;
        }
        System.out.println("No opened transaction exists for rollback");
        return false;
    }
    public Long getCreatedAt(){
        return this.createdAt;
    }
    private void setUpdatedAt(){
        this.updatedAt = (new Date()).getTime();
    }
    public Long getUpdatedAt(){
        return this.updatedAt;
    }
}