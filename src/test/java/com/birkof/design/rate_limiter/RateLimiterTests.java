package com.birkof.design.rate_limiter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
public class RateLimiterTests {
    private RateLimiter rateLimiter;
    private Long threadWaitTime;
    @BeforeEach
    public void beforeRateLimiterTest(){
        this.threadWaitTime = TimeUnit.SECONDS.toMillis(10);
        this.rateLimiter = new RateLimiter(5,this.threadWaitTime.intValue());
    }
    @Test
    public void refillTest(){
        assertThat(this.rateLimiter.refill()).isFalse();
        try{
            Thread.sleep(this.threadWaitTime);
            this.rateLimiter.allowRequest();
            assertThat(this.rateLimiter.refill()).isTrue();
        }catch (InterruptedException exception){
            assertThat(1).isEqualTo(0);
        }
    }
    @Test
    public void allowRequestTest(){
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isFalse();
    }
}
