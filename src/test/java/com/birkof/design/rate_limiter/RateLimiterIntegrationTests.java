package com.birkof.design.rate_limiter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
public class RateLimiterIntegrationTests {
    private RateLimiter rateLimiter;
    private Long threadWaitTime;
    @BeforeEach
    public void beforeRateLimiterTest(){
        this.threadWaitTime = TimeUnit.SECONDS.toMillis(10);
        this.rateLimiter = new RateLimiter(5, this.threadWaitTime.intValue());

    }
    @Test
    public void rateLimiterTest(){
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isTrue();
        assertThat(this.rateLimiter.allowRequest()).isFalse();
        assertThat(this.rateLimiter.refill()).isFalse();
        try {
            Thread.sleep(this.threadWaitTime);
            assertThat(this.rateLimiter.refill()).isTrue();
            assertThat(this.rateLimiter.allowRequest()).isTrue();
        }catch (InterruptedException exception){
            assertThat(1).isEqualTo(0);
        }
    }
}
