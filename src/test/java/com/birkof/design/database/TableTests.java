package com.birkof.design.database;

import com.birkof.design.database.exception.NoColumnException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import java.util.HashMap;

public class TableTests {
    private Table table;
    @BeforeEach
    public void beforeTableTest(){
        this.table = new Table("test_table");
    }
    @Test
    public void insertRowTest(){
        HashMap<String,String> dataA = new HashMap<>();
        HashMap<String,String> dataB = new HashMap<>();
        dataA.put("a","10");
        dataB.put("b","10");
        assertThat(this.table.insertRow(dataA)).isNotEmpty();
        assertThat(this.table.insertRow(dataB)).isNotEmpty();
    }
    @Test
    public void selectRowByIdTest(){
        HashMap<String,String> dataA = new HashMap<>();
        dataA.put("a","10");
        String rowId = this.table.insertRow(dataA);
        assertThat(this.table.selectRowById(rowId)).isInstanceOf(Row.class);
        assertThat(this.table.selectRowById("not_existing_row_id")).isNull();
    }
    @Test
    public void updateRowByIdTest(){
        HashMap<String,String> dataA = new HashMap<>();
        dataA.put("a","10");
        String rowId = this.table.insertRow(dataA);
        dataA.replace("a","20");
        assertThat(this.table.updateRowById(rowId,dataA)).isTrue();
        try{
            assertThat(this.table.selectRowById(rowId).getColumnValue("a")).isEqualTo("20");
        }catch (NoColumnException exception){
            assertThat(1).isEqualTo(0);
        }
        assertThat(this.table.updateRowById("not_existing_row_id",dataA)).isFalse();
    }
    @Test
    public void deleteRowByIdTest(){
        assertThat(this.table.deleteRowById("not_existing_row_id")).isFalse();
        HashMap<String,String> dataA = new HashMap<>();
        dataA.put("a","10");
        String rowId = this.table.insertRow(dataA);
        assertThat(this.table.deleteRowById(rowId)).isTrue();
    }
    @Test
    public void countRowsByKeyTest(){
        HashMap<String,String> dataA = new HashMap<>();
        HashMap<String,String> dataB = new HashMap<>();
        dataA.put("a","10");
        dataB.put("a","20");
        assertThat(this.table.countRowsByKey("a")).isEqualTo(0);
        String rowAId = this.table.insertRow(dataA);
        assertThat(this.table.countRowsByKey("a")).isEqualTo(1);
        String rowBId = this.table.insertRow(dataB);
        assertThat(this.table.countRowsByKey("a")).isEqualTo(2);
        this.table.deleteRowById(rowBId);
        assertThat(this.table.countRowsByKey("a")).isEqualTo(1);
    }
    @Test
    public void countRowsByValueTest(){
        HashMap<String,String> dataA = new HashMap<>();
        HashMap<String,String> dataB = new HashMap<>();
        dataA.put("a","10");
        dataB.put("b","10");
        assertThat(this.table.countRowsByValue("10")).isEqualTo(0);
        String rowAId = this.table.insertRow(dataA);
        assertThat(this.table.countRowsByValue("10")).isEqualTo(1);
        String rowBId = this.table.insertRow(dataB);
        assertThat(this.table.countRowsByValue("10")).isEqualTo(2);
        this.table.deleteRowById(rowBId);
        assertThat(this.table.countRowsByValue("10")).isEqualTo(1);
    }
    @Test
    public void startTransactionTest(){
        this.table.startTransaction();
        assertThat(this.table.commitTransaction()).isTrue();
    }
    @Test
    public void commitTransactionTest(){
        assertThat(this.table.commitTransaction()).isFalse();
        this.table.startTransaction();
        this.table.startTransaction();
        assertThat(this.table.commitTransaction()).isTrue();
        assertThat(this.table.commitTransaction()).isTrue();
        assertThat(this.table.commitTransaction()).isFalse();
    }
    @Test
    public void rollbackTransactionTest(){
        assertThat(this.table.rollbackTransaction()).isFalse();
        this.table.startTransaction();
        assertThat(this.table.rollbackTransaction()).isTrue();
        this.table.startTransaction();
        this.table.startTransaction();
        this.table.commitTransaction();
        assertThat(this.table.rollbackTransaction()).isTrue();
        assertThat(this.table.rollbackTransaction()).isFalse();
    }
}
