package com.birkof.design.database;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
public class DatabaseTests {

    private Database database;
    @BeforeEach
    public void beforeDatabaseTest(){
        this.database = new Database("test_database");
    }
    @Test
    public void createTableTest(){
        assertThat(this.database.createTable("new_table")).isTrue();
        assertThat(this.database.getTable("new_table")).isInstanceOf(Table.class);
    }
    @Test
    public void removeTable(){
        assertThat(this.database.removeTable("not_existing_table")).isFalse();
        this.database.createTable("new_table");
        assertThat(this.database.removeTable("new_table")).isTrue();
    }
    @Test
    public void getTableTest(){
        assertThat(this.database.removeTable("not_existing_table")).isFalse();
        this.database.createTable("new_table");
        assertThat(this.database.getTable("new_table")).isInstanceOf(Table.class);
    }
    @Test
    public void getTables(){
        assertThat(this.database.getTables().isEmpty()).isTrue();
        this.database.createTable("new_table_a");
        this.database.createTable("new_table_b");
        this.database.createTable("new_table_c");
        assertThat(this.database.getTables().size()).isEqualTo(3);
        assertThat(this.database.getTables().containsKey("new_table_b")).isTrue();
    }
}
