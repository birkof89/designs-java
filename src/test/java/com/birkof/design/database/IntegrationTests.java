package com.birkof.design.database;

import com.birkof.design.database.exception.NoColumnException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.HashMap;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
public class IntegrationTests {
    private DatabaseManager databaseManager;
    private Database db;
    private Table tbl;
    HashMap<String,String> dataA, dataB;
    @BeforeEach
    public void beforeIntegrationTest(){
        this.databaseManager = new DatabaseManager();
        this.databaseManager.createDatabase("my_test_db");
        this.db = databaseManager.useDatabase("my_test_db");
        db.createTable("table_a");
        this.tbl = db.getTable("table_a");
        this.dataA = new HashMap<>();
        dataA.put("a","10");
        this.dataB = new HashMap<>();
        dataB.put("a","20");
    }
    @Test
    public void transactionTest(){
        try{
            //starting transaction
            tbl.startTransaction();
            String rowIdA = tbl.insertRow(dataA);
            assertThat(tbl.selectRowById(rowIdA).getColumnValue("a")).isEqualTo("10");
            //starting another transaction in transaction
            tbl.startTransaction();
            tbl.updateRowById(rowIdA,dataB);
            assertThat(tbl.selectRowById(rowIdA).getColumnValue("a")).isEqualTo("20");
            //rolling back latest transaction
            tbl.rollbackTransaction();
            assertThat(tbl.selectRowById(rowIdA).getColumnValue("a")).isEqualTo("10");
            //rolling back latest transaction
            tbl.rollbackTransaction();
            assertThat(tbl.selectRowById(rowIdA)).isNull();
        }catch (NoColumnException exception){
            assertThat(1).isEqualTo(0);
        }
    }
}
