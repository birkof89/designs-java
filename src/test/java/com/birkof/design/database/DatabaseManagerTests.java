package com.birkof.design.database;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
public class DatabaseManagerTests {
    private DatabaseManager databaseManager;
    @BeforeEach
    public void beforeDatabaseManagerTest(){
        this.databaseManager = new DatabaseManager();
    }
    @Test
    public void createDatabaseTest(){
        assertThat(this.databaseManager.createDatabase("new_db")).isTrue();
    }
    @Test
    public void deleteDatabaseTest(){
        assertThat(this.databaseManager.deleteDatabase("not_existing_db")).isFalse();
        this.databaseManager.createDatabase("new_db");
        assertThat(this.databaseManager.deleteDatabase("new_db")).isTrue();
    }
    @Test
    public void useDatabaseTest(){
        assertThat(this.databaseManager.useDatabase("not_existing_db")).isNull();
        this.databaseManager.createDatabase("new_db");
        assertThat(this.databaseManager.useDatabase("new_db")).isInstanceOf(Database.class);
    }
    @Test
    public void saveDBStateTest(){
        assertThat(this.databaseManager.saveDBState()).isTrue();
        this.databaseManager.createDatabase("new_db");
        assertThat(this.databaseManager.saveDBState()).isTrue();
    }
    @Test
    public void restoreDBStateTest(){
        assertThat(this.databaseManager.restoreDBState()).isTrue();
        this.databaseManager.createDatabase("new_db");
        this.databaseManager.saveDBState();
        assertThat(this.databaseManager.restoreDBState()).isTrue();
    }
}
