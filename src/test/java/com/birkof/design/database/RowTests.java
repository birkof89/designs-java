package com.birkof.design.database;

import com.birkof.design.database.exception.NoColumnException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class RowTests {
    private Row row;
    @BeforeEach
    public void beforeRowTest(){
        double id = Math.random();
        this.row = new Row(Double.toString(id),new HashMap<>());
    }
    @Test
    public void setColumnValueTest(){
        this.row.setColumnValue("a","10");
        try{
            assertThat(this.row.getColumnValue("a")).isEqualTo("10");
        }catch (NoColumnException exception){
            assertThat(1).isEqualTo(0);
        }
    }
    @Test
    public void getColumnValueTest(){
        try{
            assertThat(this.row.getColumnValue("a")).isEqualTo("10");
        }catch (NoColumnException exception){
            assertThat(1).isEqualTo(1);
        }
        this.row.setColumnValue("a","10");
        try{
            assertThat(this.row.getColumnValue("a")).isEqualTo("10");
        }catch (NoColumnException exception){
            assertThat(1).isEqualTo(0);
        }
    }
}
