package com.birkof.design.elevator;

import com.birkof.design.elevator.enums.ElevatorState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.LinkedList;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
public class ElevatorTests {
    private Elevator elevator;
    @BeforeEach
    public void beforeElevatorTest(){
        this.elevator = new Elevator(ElevatorState.IDLE,0,0);
    }
    @Test
    public void addsStopIdleTest(){
        int floor = 5;
        assertThat(this.elevator.addStop(floor)).isTrue();
        assertThat(this.elevator.getStopFloorList().contains(floor)).isTrue();
    }
    @Test
    public void addsStopMovingUpTest(){
        int floorA = 5;
        int floorB = 3;
        LinkedList floorQueue = new LinkedList<Integer>();
        floorQueue.add(floorB);
        floorQueue.add(floorA);

        this.elevator.setState(ElevatorState.MOVING_UP);
        //floor A
        assertThat(this.elevator.addStop(floorA)).isTrue();
        assertThat(this.elevator.getStopFloorList().contains(floorA)).isTrue();
        //floor B
        assertThat(this.elevator.addStop(floorB));
        assertThat(this.elevator.getStopFloorList()).isEqualTo(floorQueue);
    }
    @Test
    public void addsStopMovingDownTest(){
        int floorA = 5;
        int floorB = -3;
        int floorC = -4;
        LinkedList floorQueueA = new LinkedList<Integer>();
        LinkedList floorQueueB = new LinkedList<Integer>();
        floorQueueA.add(floorB);
        floorQueueA.add(floorA);

        floorQueueB.add(floorB);
        floorQueueB.add(floorC);
        floorQueueB.add(floorA);

        this.elevator.setState(ElevatorState.MOVING_DOWN);
        //floor A
        assertThat(this.elevator.addStop(floorA)).isTrue();
        assertThat(this.elevator.getStopFloorList().contains(floorA)).isTrue();
        //floor B
        assertThat(this.elevator.addStop(floorB)).isTrue();
        assertThat(this.elevator.getStopFloorList()).isEqualTo(floorQueueA);
        //floor C
        assertThat(this.elevator.addStop(floorC)).isTrue();
        assertThat(this.elevator.getStopFloorList()).isEqualTo(floorQueueB);
    }
    @Test
    public void getReachingFloorTimeTest(){
        int floorA = 15;
        int floorB = -2;
        int floorC = -7;
        int floorD = 2;
        int resultA = 15;
        int resultB = 6;
        int resultC = 11;

        assertThat(this.elevator.addStop(floorD)).isTrue();
        assertThat(this.elevator.getReachingFloorTime(floorA)).isEqualTo(resultA);
        assertThat(this.elevator.getReachingFloorTime(floorB)).isEqualTo(resultB);
        assertThat(this.elevator.getReachingFloorTime(floorC)).isEqualTo(resultC);
    }
}
