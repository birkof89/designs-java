package com.birkof.design.elevator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
public class ElevatorControllerTests {
    private ElevatorController elevatorController;
    private int elevatorAId, elevatorBId, elevatorCId, elevatorDId;
    private final int elevatorsAmount = 4;
    @BeforeEach
    public void beforeElevatorControllerTest(){
        this.elevatorController = new ElevatorController(this.elevatorsAmount);
        this.elevatorAId = 1;
        this.elevatorBId = 2;
        this.elevatorCId = 3;
        this.elevatorDId = 4;
    }
    @Test
    public void addStopInternallyTest(){
        int stopFloorA = 5;
        int stopFloorB = 7;
        int stopFloorC = 8;
        int stopFloorD = 2;
        assertThat(this.elevatorController.addStopInternally(stopFloorA, this.elevatorAId));
        assertThat(this.elevatorController.addStopInternally(stopFloorB, this.elevatorBId));
        assertThat(this.elevatorController.addStopInternally(stopFloorC, this.elevatorCId));
        assertThat(this.elevatorController.addStopInternally(stopFloorD, this.elevatorDId));
    }
    @Test
    public void pickupElevatorTest(){
        int floorA = 4;
        int floorB = 3;
        int floorC = 7;
        int floorD = 1;
        assertThat(this.elevatorController.pickupElevator(floorA)).isEqualTo(this.elevatorAId);
        assertThat(this.elevatorController.pickupElevator(floorB)).isEqualTo(this.elevatorBId);
        assertThat(this.elevatorController.pickupElevator(floorC)).isEqualTo(this.elevatorAId);
        assertThat(this.elevatorController.pickupElevator(floorD)).isEqualTo(this.elevatorCId);
    }

    @Test
    public void saveElevatorsStateTest(){
        assertThat(this.elevatorController.saveElevatorsState()).isTrue();
    }
    @Test
    public void restoreElevatorsStateTest(){
        assertThat(this.elevatorController.restoreElevatorsState()).isTrue();
    }
}
